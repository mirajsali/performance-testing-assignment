class Customer < ActiveRecord::Base

  has_many :sales

  def total_sales
    self.sales.map(&:amount_with_tax).sum
  end

  paginates_per 500
  max_paginates_per 1000
end
