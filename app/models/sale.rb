class Sale < ActiveRecord::Base
  belongs_to :customer


  def amount_with_tax
    self.amount + self.tax
  end

  paginates_per 50
  max_paginates_per 100
end
